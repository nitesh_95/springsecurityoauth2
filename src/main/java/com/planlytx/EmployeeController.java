package com.planlytx;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {

	@Autowired
	private EmployeeServices employeeServices;

	@PostMapping("/saveemployee")
	public Employee saveData(@RequestBody Employee employee) {
		Employee save = employeeServices.saveData(employee);
		return save;
	}

	@GetMapping("/allData")
	public List<Employee> getAllData() {
		List<Employee> findAll = employeeServices.getAllData();
		return findAll;
	}

}
