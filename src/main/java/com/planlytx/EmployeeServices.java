package com.planlytx;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServices {

	@Autowired
	private EmployeeInterface employeeInterface;

	public Employee saveData(Employee employee) {
		Employee save = employeeInterface.save(employee);
		return save;
	}

	public List<Employee> getAllData() {
		List<Employee> findAll = employeeInterface.findAll();
		return findAll;
	}

}
